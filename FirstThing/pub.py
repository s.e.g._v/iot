import time
import paho.mqtt.client as mqtt_client
import random

broker = "broker.hivemq.com"


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)


client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}')
client.on_connect = on_connect
client.connect(broker)

for _ in range(100000):
    user_input = input("Enter 'd' or 'u': ").strip().lower()

    if user_input == "d" or user_input == "u":
        state = user_input
    else:
        print("Invalid input. Please enter 'd' or 'u'.")
        continue

    rnd = random.randint(4, 10)
    client.publish("lab/debug/led_state", state)
    print(f"publish state is {state}")

client.disconnect()