import time
import paho.mqtt.client as mqtt_client
import random

broker = "broker.hivemq.com"
data = ''

import serial
import time

responses = {'d': 7,
             'u': 6}

port = "COM7"
connection = serial.Serial(port, timeout=1)


def send_command(cmd: str,
                 response_len: int) -> str:
    str_resp = None
    connection.write(cmd.encode())
    if response_len != 0:
        # connection.in_waiting <-> available
        resp = connection.read(response_len)
        str_resp = resp.decode()
        return str_resp


def on_message(client, userdata, message):
    time.sleep(1)
    data = str(message.payload.decode("utf-8"))
    print("received message =", data)
    resp = send_command(data,
                        responses[data])
    print(resp)
    time.sleep(1)


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")

    else:
        print("Failed to connect, return code %d\n", rc)


client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}')
client.on_message = on_message
client.on_connect = on_connect
print(data)
client.connect(broker)
client.loop_start()
print("Subcribing")
client.subscribe("lab/debug/led_state")

time.sleep(1800)
client.disconnect()
client.loop_stop()